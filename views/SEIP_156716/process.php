<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Student Information and Marks Collection Form</title>

    <style>

        .container{
            margin-left: 40%
        }

        table{
            border: solid black 1px;
        }

        td{
            border: solid black 1px;
        }


    </style>


</head>
</body>


<?php
require_once("../../vendor/autoload.php");



$objStudent =  new \App\Student();

$objCourse  =  new \App\Course();


$objStudent->setName($_POST["name"]);
$objStudent->setStudentID($_POST["studentID"]);


$objCourse->setMarkBangla($_POST["markBangla"]);
$objCourse->setMarkEnglish($_POST["markEnglish"]);
$objCourse->setMarkMath($_POST["markMath"]);


$objCourse->setGradeBangla();
$objCourse->setGradeEnglish();
$objCourse->setGradeMath();

$name = $objStudent->getName();
$studentID = $objStudent->getStudentID();


$markBangla = $objCourse->getMarkBangla();
$markEnglish = $objCourse->getMarkEnglish();
$markMath = $objCourse->getMarkMath();

$gradeBangla = $objCourse->getGradeBangla();
$gradeEnglish = $objCourse->getGradeEnglish();
$gradeMath = $objCourse->getGradeMath();


$result=<<<RESULT

<br><br>
      <div class='container'>

        <table style='border: solid black; 2px'>

             <tr><td>Student Name: $name</td></tr>
             <tr><td>Student ID: $studentID</td></tr>
        </table>

        <br>

        <table >
             <thead>
               <th> Subject </th>
               <th> Marks </th>
               <th> Grade </th>
             </thead>

             <tr>
                <td>Bangla</td>
                <td>$markBangla</td>
                <td>$gradeBangla</td>
             </tr>

              <tr>
                <td>English</td>
                <td>$markEnglish</td>
                <td>$gradeEnglish</td>
             </tr>

              <tr>
                <td>Bangla</td>
                <td>$markMath</td>
                <td>$gradeMath</td>
             </tr>

        </table>

     </div>

RESULT;

echo $result;

file_put_contents("Result.html",$result,FILE_APPEND);

   ?>

</body>
</html>


